###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 03c - integers - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build and test a program that explores integers
###
### @author  Kobe Uyeda <kobek@hawaii.edu>
### @date    03_02_2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

# Build a datatype explorer program

HOSTNAME=$(shell hostname --short)
$(info $(HOSTNAME))

CC     = g++
CFLAGS = -g -Wall -DHOST=\"$(HOSTNAME)\"

TARGET = catPower

all: clearscreen $(TARGET)

.PHONY: clearscreen

clearscreen:
	clear

# For now, do not use Make's % expansion parameters.  Each file must have its own 
# build instructions.  We will use the % expansion later.

integers.o: catPower.cpp 
	$(CC) $(CFLAGS) -c catPower.cpp

integers: catPower.o
	$(CC) $(CFLAGS) -o $(TARGET) catPower.o

test: clearscreen $(TARGET)
	./$(TARGET) 2 j e

clean:
	rm -f *.o $(TARGET)

